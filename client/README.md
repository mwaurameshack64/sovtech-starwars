# steps to setup the application locally

- clone the git repo
```
$ git clone https://gitlab.com/mwaurameshack64/sovtech-starwars.git
```
- cd into the client directory
```

$ cd client

```
- install dependencies
```
$ yarn install
```

# How to run the Application

- Then execute the following app to run the app
```
$ yarn start
```

