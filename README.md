Using  the starwars api to create an apollo graphql server and a react SPA
SovTech FullStack interview solution


- graphql server that wraps the starwars API
  - Hosted at ``` https://matapi.work/starwars-api/graphql ``` on a server I have access to 😁😁


- Client that consumes the graphql starwars api wrapper.
  - Hosted at ``` https://sovtech-starwars.vercel.app/ ```


> **TODO**:
  - Refactor the pagination logic on the client as I realised there's a bug but the implementation can suffice for now

> **FUTURE IMPROVEMENTS**:
  - Add local state management Using Redux to reduce the network request overhead on initial render on the homepage
